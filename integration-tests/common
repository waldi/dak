# -*- mode: sh -*-
#
# © 2017 Ansgar Burchardt <ansgar@debian.org>
# License: GPL-2+
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

_escape_newline() {
    echo "$1" | perl -pe 's/\n/\\n/g'
}

assert-equal() {
  if [[ "${2}" != "${3}" ]]; then
    local actual=$(_escape_newline "$2") expected=$(_escape_newline "$3")
    echo "E: ${1} returned '${actual}' (expected: '${expected}')" >&2
    exit 1
  fi
}

fixture-package-dir() {
    packages=${DAK_ROOT:?}/tests/fixtures/packages
    echo "${packages}"
}

import-fixture-signing-key() {
    packages=$(fixture-package-dir)
    ln -sf ${packages:?}/gpg/pubring.gpg ${DAKBASE:?}/keyrings/upload-keyring.gpg
    dak import-keyring -U "%s" ${DAKBASE}/keyrings/upload-keyring.gpg
}
